
import java.io.*;
import java.net.*;

class TCPServer3 {

    public static void main(String argv[]) throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(6789);

        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            if (connectionSocket != null) {
                Client3 client = new Client3(connectionSocket);
                client.start();
            }
        }
    }
}

class Client3 extends Thread {

    private Socket connectionSocket;
    private String clientSentence;
    private String capitalizedSentence;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;

    public Client3(Socket c) throws IOException {
        connectionSocket = c;
    }

    public void run() {
        try {
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            clientSentence = inFromClient.readLine();
            if (!clientSentence.equals("*fim")) {
                System.out.printf("%s\n", clientSentence);
            }
        } catch (IOException e) {
            System.out.println("Errore: " + e);
        }
    }
}
