import java.io.*;
import java.net.*;
import java.util.Scanner;

class TCPClient1 {

    public static String sentence;
    public String modifiedSentence;
    public static int msgNumber = 0;
    public static int clientNumber = 0;

    public static void main(String argv[]) throws Exception {

        Scanner in = new Scanner(System.in);

        String msg = "";
        while ( !msg.equals("*fim")) {
            Socket clientSocket = new Socket("localhost", 6789);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            msg = in.nextLine();
            outToServer.writeBytes(msg + '\n');
            clientSocket.close();

        }
    }
}
