package TCP4;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

class TCPServer extends Thread {

    private ArrayList<Socket> connectionSocket;
//    private static String clientSentence = "Vazio";
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;

    public void run(){
        try{
        ServerSocket welcomeSocket = new ServerSocket(6789);
        this.connectionSocket = new ArrayList<>();
        Socket comSocket;
        while (true) {
            System.out.println("Aguardando mensagem do cliente.");
            comSocket = welcomeSocket.accept();
            if (comSocket != null) {
                if (connectionSocket.contains(comSocket)) {
                    System.out.println("Old socket detected.");
                    this.read(connectionSocket.indexOf(comSocket));
                } else {
                    System.out.println("Adding new socket.");
                    connectionSocket.add(comSocket);
                    this.read(connectionSocket.size()-1);
                }
            }
        }
        } catch (IOException e){
            System.out.println("Errora" + e );
        }
    }

    public void read(int i) {
        try {
            System.out.println(": Reader aguardando...");
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.get(i).getInputStream()));
            String clientSentence = inFromClient.readLine();
            if (!clientSentence.equals("*fim")) {
                System.out.printf("Rebecido %s\n", clientSentence);
                this.send(clientSentence);
            }
        } catch (Exception e) {
            System.out.println(": Errore: " + e);
        }
    }

    public synchronized void send(String message) throws IOException {
        for( int i = 0; i < this.connectionSocket.size(); i++){
            System.out.println("Enviando mensagem toUpperCase para o client: " + message.toUpperCase());
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.get(connectionSocket.size()-1).getOutputStream());
            outToClient.writeBytes(message.toUpperCase() + '\n');
        }
    }
    
    public static void main(String argv[]){
        TCPServer server = new TCPServer();
        server.start();
    }
}



