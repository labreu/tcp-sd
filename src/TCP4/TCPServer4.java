package TCP4;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class TCPServer4 {

    public static void main(String argv[]) throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(6789);
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            if (connectionSocket != null) {
                Server4 reader = new Server4(connectionSocket);
//                Server4 writer = new Server4(connectionSocket);
                reader.setName("reader");
//                writer.setName("writer");
                reader.start();
//                writer.start();
            }
        }

    }
}

class Server4 extends Thread {

    private Socket connectionSocket;
    private static String clientSentence = "Vazio";
    private String capitalizedSentence;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;

    public Server4(Socket c) throws IOException {
        connectionSocket = c;
    }

    public void run() {
        if (this.getName().equals("reader")) {
            try {
                this.read();
            } catch (Throwable ex) {
                Logger.getLogger(Server4.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (this.getName().equals("writer")) {
            this.write();
        }
    }

    public void read() throws Throwable {
        try {
            System.out.println(this.getName() + ": Reader aguardando...");
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            this.clientSentence = inFromClient.readLine();
            if (!this.clientSentence.equals("*fim")) {
                System.out.printf(this.getName() + " %s\n", this.clientSentence);
                System.out.println(this.getName() + ": Criando thread de escrita.");
                Server4 write = new Server4(connectionSocket);
                write.setName("writer");
                write.start();
                this.wait();
//                write.finalize();
            }
        } catch (Exception e) {
            System.out.println(this.getName() + ": Errore: " + e);
        }
    }

    public void write() {
        try {
            synchronized (this) {
                System.out.println(this.getName() + ": Writer aguardando...");
                System.out.println(this.getName() + ": #Enviando mensagem toUpperCase para o client: " + this.clientSentence.toUpperCase());
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                outToClient.writeBytes(this.clientSentence.toUpperCase() + '\n');
                this.notify();
            }
        } catch (Exception e) {
            System.out.println(this.getName() + ": Errore: " + e);
        }
    }
}
