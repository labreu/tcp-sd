package TCP4;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

class TCPClient {

    public static String sentence;
    public String modifiedSentence;
    public static int msgNumber = 0;
    public static int clientNumber = 0;
    
    
    public static void main(String argv[]) throws Exception {

        Scanner in = new Scanner(System.in);

        String myMsg = "";
        String serverMsg = "";
        
        BufferedReader inFromServer;
        
        while ( !myMsg.equals("*fim")) {
            Socket clientSocket = new Socket("localhost", 6789);
            System.out.println("#Leitura do teclado...");
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            myMsg = in.nextLine();
            outToServer.writeBytes(myMsg + '\n');
//            Thread.sleep(500);
            System.out.println("#Aguardando mensagem do servidor...");
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            serverMsg = inFromServer.readLine();
            System.out.println(serverMsg);
//            Thread.sleep(500);
            clientSocket.close();
            
        }
    }
}
